FROM vault:1.12.0
COPY vault-config.json /vault/config/vault-config.json
ENTRYPOINT ["vault"]
